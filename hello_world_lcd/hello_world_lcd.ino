#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2);

void setup() {
  lcd.init();
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("HELLO");
  lcd.setCursor(6, 0);
  lcd.print("WORLD");
  lcd.setCursor(12, 0);
  lcd.print("MY");
  lcd.setCursor(0, 1);
  lcd.print("NAME");
  lcd.setCursor(5, 1);
  lcd.print("IS");
  lcd.setCursor(8, 1);
  lcd.print("SASHA");

}

void loop() {
  lcd.noDisplay();
  delay(1000);
  lcd.display();
  delay(1000);
}
