#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2);
const int pot = A7;
void setup() {
  lcd.init();
  lcd.backlight();
  Serial.begin(9600);
  pinMode(pot, OUTPUT);
  
  

}

void loop() {
  analogWrite(pot, map(analogRead(pot), 0, 1023, 0, 255));
  Serial.println( map(analogRead(pot), 0, 1023, 0, 255));
  lcd.print( map(analogRead(pot), 0, 1023, 0, 255)); 
  delay(100);
  lcd.clear();
  
  
  

}
