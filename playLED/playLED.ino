#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27,16,2);
int buttons[2] = {A2, 13};

void setup() {
   lcd.init();                     
   lcd.backlight();
   lcd.setCursor(0, 1);
}

void loop() {
    int str=random(0, 2);
    lcd.setCursor(5, str);
      lcd.print(" ");
      lcd.setCursor(5, str);
      lcd.print("*");
      delay(800);
      if(digitalRead(buttons[str])==HIGH){
        lcd.setCursor(5, str);
        lcd.print(" ");
        delay(1000);
      }
      else{
        lcd.setCursor(5, str);
        lcd.print(" ");
        lcd.setCursor(0, 0);
        lcd.print("game over");
        delay(1000);
        lcd.setCursor(0, 0);
        lcd.print("           ");
      }
}
