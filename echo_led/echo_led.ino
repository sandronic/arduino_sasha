#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include <NewPing.h>
LiquidCrystal_I2C lcd(0x27, 16, 2);
const int trig = 14; 
const int echo = 15; 
NewPing sonar(trig, echo);




void setup() {
  Serial.begin(9600);
  pinMode(trig, OUTPUT);
  lcd.init();
  lcd.backlight();
  lcd.begin(16, 2);

}

void loop() {
  Serial.println(sonar.ping_cm());
  lcd.setCursor(2, 0);
  lcd.setCursor(7, -1);  
  lcd.print("Distantion");
  lcd.print(sonar.ping_cm());
  lcd.print("  ");
  delay(40);



}
