int led = 9;
int  brightnes = 0;
int fadeAmount = 5;

void setup() {
  pinMode(9, OUTPUT);
  
}

void loop() {
  analogWrite(led, brightnes);

  brightnes = brightnes + fadeAmount;

  if (brightnes == 0 || brightnes == 255) {
    fadeAmount = -fadeAmount;
  }
  delay(30);

}
